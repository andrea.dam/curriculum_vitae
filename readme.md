# Curriculum Vitae di Andrea Damiani

Semplice progetto con cui ho realizzato il mio CV.

La repository consiste semplicemente in un file HTML e un file CSS.

## Perché realizzare il mio CV così e non come un semplice documento Word? 

Perché, fresco di Hackademy di Aulab, ho pensato che mi sarei trovato meglio con la formattazione del testo.

Inoltre così ho avuto la possibilità di usare Bootstrap e le icone di Fontawesome.